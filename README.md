# Android App-Base Template para aplicaciones simples

Las aplicaciones simples se diferencian de las aplicaciones full en:

- Sólo tienen un único bloque funcional
- No requieren de Integration Layer, al no tener varios BF's.

Ésta plantilla contendrá:

- El proyecto básico de Android Studio, con estructura de carpetas predefinida, configuración de Dagger, y ejemplo de testing
- Módulo COMMONS configurado con las dependencias de ADAM.
  * Módulo para classes y assests comunes a toda la aplicación.

Para los pasos a seguir, se pueden aplicar los mismos que para la plantilla de aplicaciones full:

  * [Plantilla para crear una app](https://git.svb.lacaixa.es/mobile-docs/vision-global/blob/master/docs/android/android-creacion-proyecto.md)

## JavaDoc
  Podemos encontrar documentación generada en JavaDoc en:

 * La carpeta docs que se encuentra en la raíz del proyecto.

 * El repositorio en Gitlab: <https://git.svb.lacaixa.es/arq_android/app-base-simple/tree/master/docs>
