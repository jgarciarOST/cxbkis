/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.dagger;

import com.caixabank.adam.cxbis.features.welcome.repository.WelcomeRepository;
import com.caixabank.adam.cxbis.features.welcome.repository.impl.WelcomeRepositoryImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class AdamRepositoryModule {

  @Provides
  WelcomeRepository provideWelcomeRepository() {
    return new WelcomeRepositoryImpl();
  }
}
