/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.dashboard.view.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;


import com.caixabank.adam.arq.aarqcore.view.activity.AdamBaseActivity;
import com.caixabank.adam.arq.aarqlog.AdamLog;
import com.caixabank.adam.cxbis.CXBISApplication;
import com.caixabank.adam.cxbis.R;
import com.caixabank.adam.cxbis.features.welcome.view.fragment.WelcomeFragment;

/**
 * Entry point of application.
 * The only responsibility of this activity is to load a fragment from another BF-
 *
 */
public class DashboardActivity extends AdamBaseActivity {

  private static final String TAG = "DashboardActivity";

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.appbase_activity_dashboard);
    ((CXBISApplication) getBaseApplication()).getDaggerComponent().inject(this);

    AdamLog.debug(TAG, "onCreate");

    setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

    if (savedInstanceState == null) {
      replaceContentFrame(WelcomeFragment.newInstance());
    }
  }

  private void replaceContentFrame(Fragment fragment) {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.replace(R.id.appbase_dashboard_content_frame, fragment);
    ft.commit();
  }
}
