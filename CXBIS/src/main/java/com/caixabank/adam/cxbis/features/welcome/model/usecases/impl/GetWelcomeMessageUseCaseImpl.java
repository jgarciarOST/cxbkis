/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.model.usecases.impl;

import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;
import com.caixabank.adam.cxbis.features.welcome.model.usecases.GetWelcomeMessageUseCase;
import com.caixabank.adam.cxbis.features.welcome.repository.WelcomeRepository;

/**
 * Example of UseCase implementation.
 * It only access the repository and get the message.
 */
public class GetWelcomeMessageUseCaseImpl implements GetWelcomeMessageUseCase {

  private final WelcomeRepository welcomeRepository;

  public GetWelcomeMessageUseCaseImpl(WelcomeRepository welcomeRepository) {
    this.welcomeRepository = welcomeRepository;
  }

  @Override
  public WelcomeMessage getWelcomeMessage() {
    return welcomeRepository.getWelcomeMessage();
  }
}
