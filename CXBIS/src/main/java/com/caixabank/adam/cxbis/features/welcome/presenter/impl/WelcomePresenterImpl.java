/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.presenter.impl;

import android.os.AsyncTask;

import com.caixabank.adam.cxbis.R;
import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;
import com.caixabank.adam.cxbis.features.welcome.model.usecases.GetWelcomeMessageUseCase;
import com.caixabank.adam.cxbis.features.welcome.presenter.WelcomePresenter;

import java.lang.ref.WeakReference;

/**
 * Basic implementation of a presenter.
 * The purpose of this class is to be the intermediate between the view and the model
 */
public class WelcomePresenterImpl implements WelcomePresenter {

  private WelcomePresenter.View view;
  private GetWelcomeMessageUseCase getWelcomeMessageUseCase;

  public WelcomePresenterImpl(GetWelcomeMessageUseCase getWelcomeMessageUseCase) {
    this.getWelcomeMessageUseCase = getWelcomeMessageUseCase;
  }

  @Override
  public void setView(View view) {
    this.view = view;
  }

  @Override
  public void loadWelcomeMessage() {
    view.showProgress();
    new GetWelcomeMessageAsyncTask(getWelcomeMessageUseCase, this).execute();
  }

  @Override
  public void onDataLoaded(String message){
    if(message != null){
      view.onWelcomeMessageLoaded(message);
    }else{
      view.showError(R.string.bfbase_main_error_welcome_message);
    }
    view.hideProgress();
  }

  @Override
  public void resume() {

  }

  @Override
  public void pause() {

  }

  @Override
  public void destroy() {
    view = null;
    getWelcomeMessageUseCase = null;
  }

  private static class GetWelcomeMessageAsyncTask extends AsyncTask<Void, Void, WelcomeMessage> {

    private final GetWelcomeMessageUseCase useCase;
    private final WeakReference<WelcomePresenter> presenterRef;

    public GetWelcomeMessageAsyncTask(GetWelcomeMessageUseCase useCase, WelcomePresenter presenter) {
      this.useCase = useCase;
      this.presenterRef = new WeakReference<>(presenter);
    }

    @Override
    protected WelcomeMessage doInBackground(Void... voids) {
      return useCase.getWelcomeMessage();
    }

    @Override
    protected void onPostExecute(WelcomeMessage welcomeMessage) {
      super.onPostExecute(welcomeMessage);

      WelcomePresenter presenter = presenterRef.get();
      if (presenter != null) {
        presenter.onDataLoaded(welcomeMessage.getText());
      }
    }
  }
}
