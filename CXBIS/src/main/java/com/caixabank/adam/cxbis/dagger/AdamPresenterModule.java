/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.dagger;

import com.caixabank.adam.cxbis.features.welcome.model.usecases.GetWelcomeMessageUseCase;
import com.caixabank.adam.cxbis.features.welcome.presenter.WelcomePresenter;
import com.caixabank.adam.cxbis.features.welcome.presenter.impl.WelcomePresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class AdamPresenterModule {

  @Provides
  WelcomePresenter provideWelcomePresenter(GetWelcomeMessageUseCase getWelcomeMessageUseCase) {
    return new WelcomePresenterImpl(getWelcomeMessageUseCase);
  }
}
