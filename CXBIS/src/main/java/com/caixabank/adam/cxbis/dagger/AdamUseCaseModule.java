/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.dagger;

import com.caixabank.adam.cxbis.features.welcome.model.usecases.GetWelcomeMessageUseCase;
import com.caixabank.adam.cxbis.features.welcome.model.usecases.impl.GetWelcomeMessageUseCaseImpl;
import com.caixabank.adam.cxbis.features.welcome.repository.WelcomeRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class AdamUseCaseModule {

  @Provides
  GetWelcomeMessageUseCase provideGetWelcomeMessageUseCase(WelcomeRepository repository) {
    return new GetWelcomeMessageUseCaseImpl(repository);
  }
}
