/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.dagger;

import android.content.Context;

import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.initializer.AdamInitializerProtocol;
import com.caixabank.adam.arq.aarqlog.AdamLogger;
import com.caixabank.adam.arq.aarqlog.impl.AdamLoggerImpl;
import com.caixabank.adam.arq.aarqrest.client.AdamRestClient;
import com.caixabank.adam.arq.aarqrest.utils.AdamWebViewCookieManagerProxy;
import com.caixabank.adam.cxbis.R;
import com.caixabank.adam.cxbis.commons.utils.ConfigProperties;
import com.caixabank.adam.cxbis.initializer.CXBISInitializer;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * AdamArtifactsModule provides the basic dependencies needed in the whole application.
 * New providers that needed to be available at application level can be composed here.
 */
@Module
public class AdamArtifactsModule {

  private static final long TIMEOUT_CONNECT_MS = 3000;
  private static final long TIMEOUT_READ_MS = 3000;

  private AdamBaseApplication application;

  /**
   * Constructor.
   *
   * @param application Application context.
   */
  public AdamArtifactsModule(AdamBaseApplication application) {
    this.application = application;
  }
  @Provides
  @Singleton
  public Context providesContext(){
    return application.getApplicationContext();
  }
  /**
   *
   * @return the Logger implementation
   */
  @Provides
  @Singleton
  AdamLogger providesLogger() {
    return new AdamLoggerImpl();
  }

  /**
   *
   * @return the configuration properties instance.
   * Values are obtained from the config.xml file in the {flavor} folder, so a different
   * configuration can be obtained according to the flavor.
   */
  @Provides
  @Singleton
  ConfigProperties providesConfiguration() {
    ConfigProperties properties = new ConfigProperties();
    properties.setEnvironment(application.getString(R.string.environment));
    properties.setXmlAppsBaseUrl(application.getString(R.string.xml_apps_base_url));
    properties.setOauthBaseUrl(application.getString(R.string.oauth_base_url));
    properties.setShareCookiesWithWebview(application.getResources().getBoolean(R.bool.share_webview_cookies));
    return properties;
  }

  @Provides
  @Singleton
  OkHttpClient providesOkHttpClient(ConfigProperties config) {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

    OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .connectTimeout(TIMEOUT_CONNECT_MS, TimeUnit.MILLISECONDS)
            .readTimeout(TIMEOUT_READ_MS, TimeUnit.MILLISECONDS)
            .addInterceptor(interceptor);

    if (config.isShareCookiesWithWebview()) {
      builder.cookieJar(new AdamWebViewCookieManagerProxy());
    }
    return builder.build();
  }
  @Provides
  @Singleton AdamRestClient providesAdamrestClient(OkHttpClient okHttpClient) {
    return new AdamRestClient(okHttpClient);

  }

  @Provides
  AdamInitializerProtocol providesInitializer() {
    return new CXBISInitializer();
  }

}
