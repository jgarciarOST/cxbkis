/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.presenter;

import com.caixabank.adam.arq.aarqcore.presenter.AdamPresenter;
import com.caixabank.adam.arq.aarqcore.view.AdamBaseView;

/**
 * Account Presenter interface.
 */
public interface WelcomePresenter extends AdamPresenter<WelcomePresenter.View> {
  /**
   * This is the view contract.
   */
  interface View extends AdamBaseView {

    void onWelcomeMessageLoaded(String message);

    void showProgress();

    void hideProgress();

    void showError(int errorMessage);
  }

  void loadWelcomeMessage();
  void onDataLoaded(String message);
}
