/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.caixabank.adam.arq.aarqcore.view.fragment.AdamBasePresenterFragment;
import com.caixabank.adam.cxbis.R;
import com.caixabank.adam.cxbis.CXBISApplication;
import com.caixabank.adam.cxbis.features.welcome.presenter.WelcomePresenter;

import javax.inject.Inject;

/**
 * Welcome Fragment.
 * Extension of a BasePresenterFragment. That's why a presenter is injected
 * The main purpose of this view is to implement the presenter contract and manage
 * the visualization of the data, and hold the user input as well.
 */
public class WelcomeFragment extends AdamBasePresenterFragment<WelcomePresenter>
        implements WelcomePresenter.View {

  @Inject
  WelcomePresenter presenter;

  private View progressBar;

  private TextView welcomeMessage;

  public static WelcomeFragment newInstance() {
    return new WelcomeFragment();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    ((CXBISApplication) getBaseApplication()).getDaggerComponent().inject(this);

    setPresenter(presenter);

    getPresenter().setView(this);

    View layout = inflater.inflate(R.layout.bfbase_fragment_welcome, container, false);

    bindViews(layout);

    getPresenter().loadWelcomeMessage();

    return layout;
  }

  @Override
  public void onWelcomeMessageLoaded(String message) {
    welcomeMessage.setText(message);
  }

  @Override
  public void showProgress() {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgress() {
    progressBar.setVisibility(View.GONE);
  }

  @Override
  public void showError(int errorMessage) {
    Toast.makeText(getContext(), getString(errorMessage), Toast.LENGTH_SHORT).show();
  }

  private void bindViews(View layout) {
    progressBar = layout.findViewById(R.id.commmon_progress);
    welcomeMessage = (TextView) layout.findViewById(R.id.welcome_message);
  }
}
