/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.repository;

import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;

/**
 * Contract of this repository
 * Here we define all the methods that will treat with data.
 * In this case only to get a messages is needed , but other cases like save, load a list of items,
 * remove... should be specified here when needed
 */
public interface WelcomeRepository {
  WelcomeMessage getWelcomeMessage();
}
