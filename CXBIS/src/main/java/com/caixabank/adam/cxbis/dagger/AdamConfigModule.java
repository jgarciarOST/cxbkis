/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/

package com.caixabank.adam.cxbis.dagger;

import android.content.Context;

import com.caixabank.adam.arq.aarqrest.client.AdamRestClient;
import com.caixabank.adam.arq.arqconfig.AdamConfigManager;
import com.caixabank.adam.arq.arqconfig.initializer.data.datasource.ConfigurationCloudDataSource;
import com.caixabank.adam.arq.arqconfig.initializer.data.datasource.ConfigurationDiskDataSource;
import com.caixabank.adam.arq.arqconfig.initializer.data.policy.impl.ConfigurationRepositoryOnlyCache;
import com.caixabank.adam.arq.arqconfig.initializer.data.policy.impl.ConfigurationRepositoryWithCacheFilePolicy;
import com.caixabank.adam.arq.arqconfig.initializer.data.repository.ConfigurationRepository;
import com.caixabank.adam.arq.arqconfig.initializer.data.repository.impl.ConfigurationRepositoryImpl;
import com.caixabank.adam.cxbis.BuildConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AdamConfigModule {

    @Provides
    @Singleton
    ConfigurationDiskDataSource providesConfigurationDiskDataSource(Context context) {
        return new ConfigurationDiskDataSource(context);
    }
    @Provides
    @Singleton
    ConfigurationCloudDataSource providesConfigurationCloudDataSource(AdamRestClient adamRestClient, Context context) {
        return new ConfigurationCloudDataSource(adamRestClient, context);
    }
    @Provides
    @Singleton
    ConfigurationRepositoryWithCacheFilePolicy providesConfigurationRepositoryPolicyCahceWithCloud(ConfigurationCloudDataSource configurationCloudDataSource, ConfigurationDiskDataSource configurationDiskDataSource) {
        return new ConfigurationRepositoryWithCacheFilePolicy(configurationCloudDataSource,configurationDiskDataSource);
    }

    @Provides
    @Singleton
    ConfigurationRepositoryOnlyCache providesConfigurationRepositoryPolicyCache(ConfigurationCloudDataSource configurationCloudDataSource, ConfigurationDiskDataSource configurationDiskDataSource) {
        return new ConfigurationRepositoryOnlyCache(configurationDiskDataSource);
    }

    @Provides
    @Singleton
    ConfigurationRepositoryImpl providesConfigurationRepositoryWithCache(ConfigurationRepositoryOnlyCache configurationRepositoryPolicy) {
        return new ConfigurationRepositoryImpl(configurationRepositoryPolicy);
    }

    @Provides
    @Singleton
    ConfigurationRepository providesConfigurationRepositoryWithCacheAndCloud(ConfigurationRepositoryWithCacheFilePolicy configurationRepositoryPolicy) {
        return new ConfigurationRepositoryImpl(configurationRepositoryPolicy);
    }

    @Provides
    @Singleton
    AdamConfigManager providesAdamConfigManager(ConfigurationRepositoryImpl configurationRepositoryWithCache,
                                                ConfigurationRepository configurationRepositoryWithCacheAndCloud) {
        return new AdamConfigManager(configurationRepositoryWithCacheAndCloud, configurationRepositoryWithCache, BuildConfig.ENCRYPT_KEY_ID);
    }
}
