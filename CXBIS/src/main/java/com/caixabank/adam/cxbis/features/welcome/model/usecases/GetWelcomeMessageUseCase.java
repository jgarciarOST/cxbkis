/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.model.usecases;

import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;

/**
 * Contract of the use case
 */
public interface GetWelcomeMessageUseCase {

  WelcomeMessage getWelcomeMessage();
}
