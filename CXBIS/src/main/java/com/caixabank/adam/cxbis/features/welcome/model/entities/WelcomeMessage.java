/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.model.entities;

/**
 * Pojo entity, to show how an entity in the module layer should be.
 * All entities must be independent of any other layer
 */
public class WelcomeMessage {

  private String text;

  public WelcomeMessage(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }
}
