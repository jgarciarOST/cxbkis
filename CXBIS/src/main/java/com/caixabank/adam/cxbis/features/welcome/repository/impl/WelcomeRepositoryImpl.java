/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.repository.impl;

import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;
import com.caixabank.adam.cxbis.features.welcome.repository.WelcomeRepository;

/**
 * Repository pattern implementation.
 * Example of a simple repository.
 * More robust implementations will be developer as needed.
  */
public class WelcomeRepositoryImpl implements WelcomeRepository {

  public WelcomeRepositoryImpl() {
  }

  /**
   * Gets the welcome Message.
   *
   * @return Welcome message.
   */
  @Override
  public WelcomeMessage getWelcomeMessage() {
    return new WelcomeMessage("Welcome Message");
  }
}
