/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.dagger;

import com.caixabank.adam.cxbis.CXBISApplication;
import com.caixabank.adam.cxbis.features.dashboard.view.activity.DashboardActivity;
import com.caixabank.adam.cxbis.features.welcome.view.fragment.WelcomeFragment;
import com.caixabank.adam.cxbis.initializer.CXBISInitializer;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Main component for dependencies injection
 */
@Singleton
@Component(modules = {
        AdamArtifactsModule.class,
        AdamPresenterModule.class,
        AdamRepositoryModule.class,
        AdamUseCaseModule.class,
        AdamConfigModule.class})

public interface AdamComponent {
  void inject(CXBISApplication application);
  void inject(DashboardActivity activity);
  void inject(CXBISInitializer initializer);
  void inject(WelcomeFragment welcomeFragment);
}
