/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis;


import com.caixabank.adam.arq.aarqcore.AdamBaseApplication;
import com.caixabank.adam.arq.aarqcore.initializer.AdamInitializerProtocol;
import com.caixabank.adam.cxbis.dagger.AdamArtifactsModule;
import com.caixabank.adam.cxbis.dagger.AdamComponent;
import com.caixabank.adam.cxbis.dagger.DaggerAdamComponent;

import javax.inject.Inject;

/**
 * Base application class implementation.
 */
public class CXBISApplication extends AdamBaseApplication {

    private AdamComponent daggerAppComponent;

    @Inject
    AdamInitializerProtocol applicationInitializer;

    @Override
    public void initializeInjection() {
        //Main dagger component, do not touch
        daggerAppComponent = DaggerAdamComponent.builder()
                .adamArtifactsModule(new AdamArtifactsModule(this))
                .build();
        daggerAppComponent.inject(this);
    }

    @Override
    public void initializeApplication() {
        setApplicationInitializer(applicationInitializer);
    }

    /**
     * Register the subcomponents of other functional modules .
     * this method is called at the onCreate callback of the application
     * The order of registration is important since it will be the same order that initializers
     * will be executed
     */
    @Override
    public void registerApplicationModules() {
    }

    /**
     * @return AdamComponent , the main dagger component
     */

    public AdamComponent getDaggerComponent() {
        return daggerAppComponent;
    }
}
