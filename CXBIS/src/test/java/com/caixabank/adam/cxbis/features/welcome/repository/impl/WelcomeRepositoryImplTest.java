/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/

package com.caixabank.adam.cxbis.features.welcome.repository.impl;

import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;
import com.caixabank.adam.cxbis.features.welcome.repository.WelcomeRepository;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

public class WelcomeRepositoryImplTest {

    private WelcomeRepository welcomeRepository;

    /**
     * Tests that repository returns correct message
     *
     * @throws Exception
     */
    @Test
    public void get_welcome_message_returns_correct_message() throws Exception {
        //Given
        welcomeRepository = new WelcomeRepositoryImpl();

        //When
        WelcomeMessage welcomeText = welcomeRepository.getWelcomeMessage();

        //Then
        assertThat(welcomeText, instanceOf(WelcomeMessage.class));
        Assert.assertEquals("Welcome Message", welcomeText.getText());
    }
}