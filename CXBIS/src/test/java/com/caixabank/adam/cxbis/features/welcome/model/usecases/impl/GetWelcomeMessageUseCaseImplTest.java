/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.features.welcome.model.usecases.impl;

import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;
import com.caixabank.adam.cxbis.features.welcome.repository.WelcomeRepository;
import com.caixabank.adam.cxbis.features.welcome.repository.impl.WelcomeRepositoryImpl;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GetWelcomeMessageUseCaseImplTest {

  /**
   * Tests that use case uses repository and it returns correct message
   */
  @Test
  public void get_welcome_message_should_use_repository_returns_correct_message() {
    //Given
    WelcomeRepository repository = new WelcomeRepositoryImpl();

    //When
    WelcomeMessage result = new GetWelcomeMessageUseCaseImpl(repository).getWelcomeMessage();

    //Then
    assertNotNull(result);
    assertEquals("Welcome Message", result.getText());
  }
}
