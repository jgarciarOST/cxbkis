/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/

package com.caixabank.adam.cxbis.features.welcome.presenter.impl;

import com.caixabank.adam.cxbis.R;
import com.caixabank.adam.cxbis.features.welcome.model.entities.WelcomeMessage;
import com.caixabank.adam.cxbis.features.welcome.model.usecases.GetWelcomeMessageUseCase;
import com.caixabank.adam.cxbis.features.welcome.model.usecases.impl.GetWelcomeMessageUseCaseImpl;
import com.caixabank.adam.cxbis.features.welcome.presenter.WelcomePresenter;
import com.caixabank.adam.cxbis.features.welcome.repository.WelcomeRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WelcomePresenterImplTest {

    @Mock
    private WelcomeRepository repository;
    @Mock
    private WelcomePresenter.View view;

    private WelcomePresenter welcomePresenter;
    private GetWelcomeMessageUseCase getWelcomeMessageUseCase;

    /**
     * Setup WelcomeMessage use case, mocked repository,
     * mocked view and presenter
     *
     */
    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        getWelcomeMessageUseCase = new GetWelcomeMessageUseCaseImpl(repository);
        welcomePresenter = new WelcomePresenterImpl(getWelcomeMessageUseCase);
        welcomePresenter.setView(view);
    }

    /**
     * Remove references when test ends
     *
     */
    @After
    public void tearDown() {
        welcomePresenter = null;
        getWelcomeMessageUseCase = null;
    }

    /**
     * Tests that presenter could get a correct message,
     * and onWelcomeMessageLoaded is called with it.
     *
     * @throws Exception
     */
    @Test
    public void load_welcome_message_correct_message_calls_onWelcomeMessageLoaded_with_message() throws Exception {
        //Given
        when(repository.getWelcomeMessage()).thenReturn(new WelcomeMessage("test message"));

        //When
        view.showProgress();
        verify(view).showProgress();

        view.onWelcomeMessageLoaded("test message");

        view.hideProgress();
        verify(view).hideProgress();

        //Then
        verify(view, never()).onWelcomeMessageLoaded(null);
        verify(view).onWelcomeMessageLoaded("test message");
    }

    /**
     * Tests that an error message return (null) forces presenter
     * to call showError function from view with an error message.
     *
     * @throws Exception
     */
    @Test
    public void load_welcome_message_returns_null_calls_showError_with_error_message() throws Exception {
        //When
        view.showProgress();
        verify(view).showProgress();

        welcomePresenter.onDataLoaded(null);

        verify(view).hideProgress();

        //Then
        verify(view).showError(R.string.bfbase_main_error_welcome_message);
        verify(view, never()).onWelcomeMessageLoaded(anyString());
    }
}