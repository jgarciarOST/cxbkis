/*
---------------- Component generat per l’Arquitectura d’Aplicacions Mòbils (ADAM) -------------------
-------------------- (C) COPYRIGHT "CaixaBank" -----------------------
---------------------------------------------------------------------
*/
package com.caixabank.adam.cxbis.commons.utils;

public class ConfigProperties {

  private String environment;
  private String xmlAppsBaseUrl;
  private String oauthBaseUrl;

  private boolean shareCookiesWithWebview;

  public ConfigProperties() {
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

  public String getXmlAppsBaseUrl() {
    return xmlAppsBaseUrl;
  }

  public void setXmlAppsBaseUrl(String xmlAppsBaseUrl) {
    this.xmlAppsBaseUrl = xmlAppsBaseUrl;
  }

  public String getOauthBaseUrl() {
    return oauthBaseUrl;
  }

  public void setOauthBaseUrl(String oauthBaseUrl) {
    this.oauthBaseUrl = oauthBaseUrl;
  }

  public boolean isShareCookiesWithWebview() {
    return shareCookiesWithWebview;
  }

  public void setShareCookiesWithWebview(boolean shareCookiesWithWebview) {
    this.shareCookiesWithWebview = shareCookiesWithWebview;
  }
}
