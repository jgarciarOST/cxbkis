# Presenter

Este package contiene clase e interface que usa la capa de "presenter" dentro del patrón MVP.

Ejemplo de implementación:

<pre>        
│ presenter
│  ├─ impl
│  │   └── WelcomePresenterImpl.java
│  ├── WelcomePresenter.java
│   
</pre>